let menuState = "closed";

function openNav() {
    document.querySelector('nav').style.width = "100%";
}

function closeNav() {
    document.querySelector('nav').style.width = "50px";
}

function toggleMenu() {
    if (menuState == 'closed') {
        openNav();
        menuState = 'open';
        document.querySelector('#navButton i').innerHTML = 'close';
    } else {
        closeNav();
        menuState = 'closed';
        document.querySelector('#navButton i').innerHTML = 'menu';
    }
}